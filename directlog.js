console.clear()

const rp = require('request-promise')
const cheerio = require('cheerio')
const fs = require('fs')
const bot = require('./bot.js')

let getRows = fs.readFileSync('db.json')
let dataRow = JSON.parse(getRows)

const saveIt = (newData) => {
  const toString = JSON.stringify(newData)
  fs.writeFileSync('db.json', toString)
}

const options = {
  uri: 'https://www.directlog.com.br/track_individual/index.asp?fase=0&tipo=0&numtracking=20180006725706',
  transform: function (body) {
    return cheerio.load(body)
  }
}

rp(options)
  .then(($) => {
    let rows = []
    $('[onmouseout]').each((i, item) => {
      const row = $(item).find('td').text()
      rows.push(row)
    })
    botTalk(rows)
  })
  .catch((err) => {
    console.log(err)
  })

function botTalk (rows) {
  if (rows.length > dataRow.rows) {
    bot(rows)
  }
}
