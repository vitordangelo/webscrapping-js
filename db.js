const fs = require('fs')

const db = (data) => {
  return {
    save: (data) => {
      // const jsonDb = {
      //   rows: data
      // }
      // const toString = JSON.stringify(jsonDb)
      // fs.writeFileSync('db.json', toString)
      // console.log(toString)

      setTimeout(() => {
        const jsonDb = {
          rows: data
        }
        const toString = JSON.stringify(jsonDb)
        fs.writeFileSync('db.json', toString)
        console.log(toString)
      }, 4000)
    }
  }
}

module.exports = db
